import { URL, API_KEY, HASH } from '../../config'

export const getHeroAction = async (parsed) => {    
    let dataJson = {}    
    if (parsed && Object.keys(parsed).length !== 0) {        
        try {
            const data = await fetch(`${URL}characters?nameStartsWith=${parsed.character}&ts=1&apikey=${API_KEY}&hash=${HASH}`)
            dataJson = await data.json()
            const heroList = getHeroList(dataJson.data.results)
            return heroList
        } catch (error) {            
            return false
        }
    } else {
        try {
            const data = await fetch(`${URL}characters?limit=100&ts=1&apikey=${API_KEY}&hash=${HASH}`)
            dataJson = await data.json()
            const heroList = getHeroList(dataJson.data.results)    
            const random = parseInt(Math.random() * heroList.length);        
            let heroOne = []
            heroOne.push(heroList[random])    
            return heroOne
        } catch (error) {            
            return false
        }
    }  
}

export const getHeroByNameAction = async (value) => {    
    let dataHeroJson = {}
    let dataComicJson = {}
    try {
        const dataHero = await fetch(`${URL}characters?nameStartsWith=${value}&ts=1&apikey=${API_KEY}&hash=${HASH}`)
        const dataComic = await fetch(`${URL}/comics?titleStartsWith=${value}&ts=1&apikey=${API_KEY}&hash=${HASH}`)        
        dataHeroJson = await dataHero.json()
        dataComicJson = await dataComic.json()
    } catch (error) {        
        return false
    }

    let heroList = []
    let comicList = []
    if (dataHeroJson.data) heroList = getHeroList(dataHeroJson.data.results)    
    if (dataComicJson.data) heroList = getComicList(dataComicJson.data.results)
    
    const finalList = shuffle([...heroList, ...comicList])
    return finalList    
}

const getHeroList = (data) => {
    const heroList = data.map((x) => {
        const star = x.events.available = x.events.available === 1
        return { id: x.id, name: x.name, thumbnail: x.thumbnail, star, comics: x.comics.items }
    });
    return heroList
}

const getComicList = (data) => {    
    const comicList = data.map((x) => {        
        const star = x.events.available = x.events.available === 1
        const image = x.images.length ? x.images[0].path + '.' + x.images[0].extension : false
        const publishedDate = x.dates[0].date
        let writer = ''
        let penciler = ''
        let coverArtist = ''
        x.creators.items.forEach(element => {
            if (element.role === 'writer') writer = element.name
            if (element.role === 'penciller') penciler = element.name
            if (element.role === 'penciller (cover)') coverArtist = element.name
        });

        return { id: x.id, name: x.title, description: x.description, image, star, publishedDate, thumbnail: x.thumbnail, writer, penciler, coverArtist, comics: false }
    });
    return comicList
}

const shuffle = (a) => {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

export const getComicAction = (comics) => {
    const data = comics.map(element => {
        const urlHttps = element.resourceURI.replace('http', 'https')
        const url = `${urlHttps}?ts=1&apikey=${API_KEY}&hash=${HASH}`        
        return url
    });
    return data;
}

export const getComicByUrlAction = async (idName) => {    
    let dataComicJson = {}
    try {
        const dataComic = await fetch(`${URL}characters/${idName[0]}/comics?titleStartsWith=${idName[1]}&ts=1&apikey=${API_KEY}&hash=${HASH}`)                            
        dataComicJson = await dataComic.json()
    } catch (error) {        
        return false
    }    
    
    if (!dataComicJson.data.results) {        
        const comic = dataComicJson.data.results[0]    
        const image = comic.images[0] ? comic.images[0].path + '.' + comic.images[0].extension : false
        const publishedDate = comic.dates[0].date
        let writer = ''
        let penciler = ''
        let coverArtist = ''
        comic.creators.items.forEach(element => {
            if (element.role === 'writer') writer = element.name
            if (element.role === 'penciller') penciler = element.name
            if (element.role === 'penciller (cover)') coverArtist = element.name
        });
        const detailComic = {
            id: comic.id,
            image,
            title: comic.title,
            publishedDate,
            writer,
            penciler,
            coverArtist,
            description: comic.description                
        }    
        return detailComic
    } else return false   
}

export const getComicByNameHeroAndTitleAction = async (parsed) => {    
    let { comic, name } = parsed
    if (typeof(name) === 'string') { comic = [comic]; name = [name] }
     
    let arrayDataCharactersJson = []
    try {
        const arrayDataCharacters = await Promise.all(name.map(async (file) => {
            return await fetch(`${URL}characters?name=${file}&ts=1&apikey=${API_KEY}&hash=${HASH}`)			
        }));

        arrayDataCharactersJson = await Promise.all(arrayDataCharacters.map(async (file) => {
            return await file.json()			
        }));
    } catch (error) {        
        return false
    }        
    let newArrayDataCharactersJson = []

    for (const [i, iterator] of arrayDataCharactersJson.entries()) {        
        if(iterator.data) {
            const x = iterator.data.results[0]                
            if (x) {                    
                newArrayDataCharactersJson.push(x)
                const { id, thumbnail } = x
                const star = x.events.available = x.events.available === 1

                let dataJsonComicByIdHero = {}
                try {           
                    const data = await fetch(`${URL}characters/${id}/comics?titleStartsWith=${comic[i]}&ts=1&apikey=${API_KEY}&hash=${HASH}`)                                   
                    dataJsonComicByIdHero = await data.json()
                } catch (error) {                    
                    return false
                }

                if (dataJsonComicByIdHero.data) {
                    const hero = [{ id, name, thumbnail, star, comics: dataJsonComicByIdHero.data.results }]            
                    return hero
                }
            }            
        }                             
    }
    return []    
}
