import React, { Component } from 'react'
import { withRouter } from 'react-router-dom';
import { URL } from '../../config'
import { getHeroAction, getHeroByNameAction, getComicAction, getComicByUrlAction, getComicByNameHeroAndTitleAction } from './homeAction'
import Navbar from '../../components/navBar'
import Card from '../../components/card'
import Alert from '../../components/alert'
import Modal from "../../components/modalComic";

const queryString = require('query-string');

class HomePage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            heroList: [],
            heroName: '',
            comicList: [],
            modal: false,
            spinner: true,
            alert: false
        };
    }

    async componentDidMount() {        
        const parsed = queryString.parse(this.props.location.search);             
        this.getHero(parsed)        
    }

    getHero = async (parsed) => {
        const heroList = await getHeroAction(parsed)
        if (!heroList) {
            return this.setState({ alert: true })
        }        
        this.setState({ heroList, spinner: false })
    }    

    handleInputSearch = async (value) => {        
        let heroList = null

        if (value.length > 0) {          
            this.setState({ spinner: true })
            const startsValue = 'https://www.marvel.com/comics/issue/'
            
            if (value.startsWith(startsValue)) {                
                const withoutUrl = value.replace(startsValue, '')                
                const idName = withoutUrl.split('/')
                const detailComic = await getComicByUrlAction(idName)                  
                if(detailComic.id) {
                    this.props.history.push({
                        pathname: `/detailComic/:${detailComic.id}`,
                        state: detailComic
                    });
                }                
            } else {  
                const startsValue = `${URL}characters?`
                if (value.startsWith(startsValue)) {                    
                    const withoutUrl = value.replace(startsValue, '')                    
                    const parsed = queryString.parse(withoutUrl);                    
                    heroList = await getComicByNameHeroAndTitleAction(parsed)                    
                } else {
                    heroList = await getHeroByNameAction(value)                    
                }                
            }            
            
            if (!heroList) {
                return this.setState({ alert: true })
            }            
            return this.setState({ heroList, spinner: false })
                      
        } else {
            this.getHero()
        }        
    }

    toggleClose = () => {
        this.setState((prevState) => ({
            modal: !prevState.modal
        }));
    }

    toggleOpen = (heroName, comics) => {
        const comicList = getComicAction(comics)        
        this.setState((prevState) => ({
            modal: !prevState.modal,
            heroName,
            comicList
        }));
    }

    render() {
        const { heroList, heroName, comicList, modal, spinner, alert } = this.state;        
        return (
            <div>                
                <Navbar handleInputSearch={this.handleInputSearch} ableSearch={true} />
                <div className="home-page">
                    {!spinner ? (
                        <>
                            <Modal toggle={this.toggleClose} modal={modal} heroName={heroName} comicList={comicList} />
                            {
                                heroList.length > 0 ? heroList.map((hero, i) => {
                                    return (
                                        <Card key={i} {...hero} toggle={this.toggleOpen} />
                                    )
                                }) : <></>
                            }
                        </>
                    ) : (
                            <>
                                {!alert ? (
                                    <div className="sp sp-3balls"></div>
                                ) : (
                                        <Alert message={'Something is Wrong with the Api'} getHero={this.getHero()} />
                                    )
                                }
                            </>
                        )
                    }
                </div>
            </div>
        )
    }
}

export default withRouter(HomePage);