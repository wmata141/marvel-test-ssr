import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Navbar from '../../components/navBar';

const Title = styled.h5`
	position: relative;
    font-family: 'Montserrat Semi Bold'; 
    font-size: 25px;
    line-height: 21px;
	text-align: left;      
	color: #3E3E3E;	 
    margin: 0px;      
`

const Detail = styled.h6`
	position: relative;
	font-family: Montserrat Light; 		
    font-size: 18px;
    line-height: 21px;
    text-align: left;      
	color: #3E3E3E;
    margin: 0px;        
`

const Description = styled.h6`
	position: relative;
    font-family: Montserrat Light;
    font-size: 18px;
    line-height: 21px;
    font-weight: lighter;
	text-align: left;      
	color: #4D4D4D;
    margin: 0px;    
`

const DetailPage = (props) => {    
    const { image, title, publishedDate, writer, penciler, coverArtist, description } = props.location.state
    return (
        <div>            
            <Navbar ableSearch={false}/>
            <div className="home-page" style={{ alignItems: 'flex-start' }}>
                <div className="image-detail">
                    <img src={image} alt="Star" />
                </div>
                <div className="colum-detail" >
                    <div style={{ marginBottom: '30px' }}>
                        <Title>{title}</Title>
                    </div>
                    <div style={{ marginTop: '30px' }}>
                        <Detail>Publishead: {publishedDate}</Detail>
                        <Detail>Writer: {writer}</Detail>
                        <Detail>Pendiler: {penciler}</Detail>
                        <Detail>Cover Artist{coverArtist}</Detail>
                    </div>
                    <div style={{ marginTop: '30px' }}>
                        <Description>{description}</Description>
                    </div>
                </div>
            </div>
        </div>
    )
}
  
DetailPage.propTypes = {
    location: PropTypes.object.isRequired,
};

export default DetailPage