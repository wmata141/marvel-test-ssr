import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import marvelLogo from '../assets/img/marvel_logo.png';
import starWhite from '../assets/img/star_white.png';
import starBlack from '../assets/img/star_black.png';

const NavBar = ({ ableSearch, handleInputSearch }) => {
    const [search, setSearch] = useState('');
    const [star, setStar] = useState(false);    
    
    useEffect(() => {
        if (search) {  
            const searchList = JSON.parse(localStorage.getItem("searchs")); 
            if (searchList) {
                searchList.includes(search) ? setStar(true) : setStar(false)        
            }        
        }        
    }, [search]);
    
    let history = useHistory();
    const handleClick = () => {
        history.push("/");
    }

    const saveSearch = () => {        
        const input = document.getElementById("input").value        
        if (!input) return false

        if (star) {            
            const searchs = JSON.parse(localStorage.getItem("searchs"));            
            const indice = searchs.indexOf(input); // obtenemos el indice
            searchs.splice(indice, 1); // 1 es la cantidad de elemento a eliminar                        
            localStorage.setItem('searchs', JSON.stringify(searchs)); // ingresamos nuevo arreglo
            setStar(false)
        } else {
            if (!localStorage.getItem('searchs')) {                              
                localStorage.setItem('searchs', JSON.stringify([input]));                 
            } else {
                const searchs = JSON.parse(localStorage.getItem("searchs"));                
                searchs.push(input)
                localStorage.setItem('searchs', JSON.stringify(searchs));
            }            
            setStar(true)            
        }    
    }

    const twoSearch = (value) => {
        setSearch(value)
        handleInputSearch(value)
    }

    let starImg = null 
    star ? starImg = starBlack : starImg = starWhite
    
    return (
        <header className='navbar'>
            <div className='navbar-title navbar-item'>
                <img src={marvelLogo} alt="MARVEL" width={70} onClick={handleClick} />
                {
                    ableSearch ? (
                        <div className="input-icono">
                            <input
                                placeholder='Buscar'
                                onChange={(e) => twoSearch(e.target.value)}
                                id="input"
                                value={search}
                            />
                        </div>
                    ) : <></>
                }
            </div>
            <div onClick={() => saveSearch()} className='navbar-item'>
                <img src={starImg} alt="Star" width={25} height={25} />
            </div>
        </header>
    )
}

NavBar.defaultProps = {
    handleChange: PropTypes.func
};

NavBar.propTypes = {
    ableSearch: PropTypes.bool.isRequired,
    handleChange: PropTypes.func.isRequired,
};

export default NavBar