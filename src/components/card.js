import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useHistory } from "react-router-dom";
import starWhite from '../assets/img/star_white.png';
import starBlack from '../assets/img/star_black.png';

const Card = ( hero ) => {    
    const image = `${hero.thumbnail.path}.${hero.thumbnail.extension}`
    const iconStar = hero.star ? starBlack : starWhite    
    let history = useHistory();
    
    const handleClick = (name, comics) => {        
        if (comics) {                 
            hero.toggle(name, comics)
        } else {                                 
            const detailComic = {
                image: hero.image,
                title: hero.name,
                publishedDate: hero.publishedDate,
                writer: hero.writer,
                penciler: hero.penciler,
                coverArtist: hero.coverArtist,
                description: hero.description                
            }            
            history.push({
                pathname: `/detailComic/:${hero.id}`,
                state: detailComic
            });
        }
    }

    return (
        <CardHero onClick={() => handleClick(hero.name, hero.comics)} >
            <Picture src={image} alt={hero.name} />
            <Icon src={iconStar} alt="Star" />
            <Name>{hero.name}</Name>
        </CardHero >
    )
}

Card.propTypes = {
    thumbnail: PropTypes.object.isRequired,
    star: PropTypes.bool.isRequired,
    name: PropTypes.string.isRequired,
    comics: PropTypes.any.isRequired,
    toggle: PropTypes.func.isRequired
};

export default Card

const CardHero = styled.div`
    position: relative;
    margin: 1rem;
    width: 20%;
    min-width: 256px;
    min-height: 380px;
    max-width: 256px;
    max-height: 380px;      
    box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
    transition: all 0.3s cubic-bezier(.25,.8,.25,1);
    cursor: pointer;
    &:hover {
        box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);        
    }    
`
const Picture = styled.img`
    position: absolute;
    width: 100%;
    height: 100%;
    border-radius: 4px;   
`;

const Icon = styled.img`
    position: absolute;    
    filter: invert(1);    
    margin-top: 14px;
    margin-left: 220px;
    height: 19px;
    width: 20px;
`;

const Name = styled.h5`
    position:absolute;
    text-align: left;    
    font-family: 'SFUIText-Bold';
    font-size: 19px;
    line-height: 23px;
    letter-spacing: -0.25px;
    color: #FFFFFF;
    opacity: 1;    
    margin-top: 330px; 
    margin-left: 23px;
    height: 23px;  
`