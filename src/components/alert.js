import React from 'react';
import styled from 'styled-components';

const AlertToast = styled.div`   
    display: flex;    
    position: fixed;       
    left: 0;
    right: 0;
    bottom: 0;
    top: 0;    
    width: 100%;
    height: 100%;       
    z-index: 1;
    background: #00000076 0% 0% no-repeat padding-box;
    box-shadow: 0px 2px 13px #000000;
    transition: opacity 0.4s ease-in-out;    
    opacity: 1;   
`

const Inner = styled.div`
    display: flex;
    flex-direction: column;
    margin: auto;
    text-align: left;
    padding: 15px;     
    min-width: 256px;
    min-height: 110px;
    width: 437px;
    height: 110px;    
    background-color: #f44336;
    border-radius: 8px;
    cursor: pointer;
    color: white;
`

function Alert({ message, getHero }) {
    return (
        <AlertToast onClick={() => getHero} >
            <Inner>
                <strong>{message}</strong>
                <h5>Pres here to try again</h5>
            </Inner>
        </AlertToast>
    );
}

export default Alert;