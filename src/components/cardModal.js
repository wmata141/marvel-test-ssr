import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';
import starWhite from '../assets/img/star_white.png';
import starBlack from '../assets/img/star_black.png';
import notFound from '../assets/img/img_not_found.png';

const NameStar = styled.h6`   	
    font-family: Montserrat Semi Bold;
    font-size: 12px;
    line-height: 15px;
    color: #4D4D4D;
`

const Description = styled.h6`
    font-family: Montserrat Light;
    font-size: 12px;
    line-height: 15px;
    color: #4D4D4D;
    font-weight: lighter;
    max-height: 65px;
    text-overflow: ellipsis;
    overflow: hidden;
    margin: 0;
`

class CardModal extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id: null,
            title: '',
            description: '',
            image: '',
            star: false,
            publishedDate: '',
            writer: '',
            penciler: '',
            coverArtist: ''
        }
    }

    componentDidMount() {
        this.getData(this.props.comic)
        this.setBodyScroll(true)
    }

    componentWillUnmount() {
        this.setBodyScroll(false)
    }

    setBodyScroll(hidden) {
        // Esconder el scroll cuando se abre la modal
        document.body.style['overflow-y'] = hidden ? 'hidden' : 'auto'
    }

    getData = async (resourceURI) => {
        const { id, title, description, images, events, creators, dates } = resourceURI
        const image = images[0] ? images[0].path + '.' + images[0].extension : notFound               
        const star = events.available = events.available === 1
        const publishedDate = dates[0].date
        let writer = ''
        let penciler = ''
        let coverArtist = ''

        creators.items.forEach(element => {
            if (element.role === 'writer') writer = element.name
            if (element.role === 'penciller') penciler = element.name
            if (element.role === 'penciller (cover)') coverArtist = element.name
        });
        this.setState({ id, title, description, image, star, publishedDate, writer, penciler, coverArtist })
    }

    detailHandle = (id) => {
        this.props.history.push({
            pathname: `/detailComic/:${id}`,
            state: this.state
        });
    }

    render() {
        const { id, title, description, image, star } = this.state
        const iconStar = star ? starBlack : starWhite
        return (
            <div className="container modal-body-children" onClick={() => this.detailHandle(id)}>
                <div className="modal-body-image">
                    <img src={image} alt="Star" />
                </div>
                <div className="modal-body-text">
                    <div className="modal-body-text-title">
                        <NameStar>{this.props.name}</NameStar>
                        <img src={iconStar} alt="Star" />
                    </div>
                    <div className="modal-body-text-description">
                        <Description>{title}' '{description}</Description>
                    </div>
                </div>          
            </div>
        );
    }
}

CardModal.propTypes = {
    comic: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
};

export default withRouter(CardModal)


