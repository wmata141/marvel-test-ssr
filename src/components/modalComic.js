import React from "react";
import PropTypes from 'prop-types';
import styled from 'styled-components';
import CardModal from './cardModal';
import { getData } from './componentsAction';
import hydra from '../assets/img/hydra.png';

const HeroName = styled.h5`
	position: absolute;
	font-family: Montserrat Semi Bold; 	
	margin-top: 25px;
	text-align: left;      
	color: #515151;
	opacity: 1;
	font-size: 25px;
	line-height: 30px;
`
const X = styled.h6`   	
	font-family: Montserrat Light;
	font-size: 20px;
	line-height: 24px;	
	margin-top: 12px;
	margin-left: 95%;    	
	color: #4D4D4D;
	opacity: 1;
	cursor: pointer;
	letter-spacing: 0px;
	font-weight: 100;	
`
const NameStar = styled.h6`   	
    font-family: Montserrat Semi Bold;
    font-size: 12px;
    line-height: 15px;
    color: #4D4D4D;
`

class Modal extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			comicListResult: [],
			spinner: true
		}
	}

	async componentDidUpdate(prevProps) {
		const { comicList } = this.props
		if (prevProps.comicList !== comicList) {			
			const comicListResult = await getData(comicList)
			this.setState({ comicListResult, spinner: false })
		}
	}
	
	render() {
		const { modal, heroName, toggle } = this.props
		const { spinner, comicListResult } = this.state
		if (!modal) return null
					
		return (
			<div className="modal-background">
				<div className="modal-inner">
				<div>
					<HeroName>{heroName}</HeroName>
					<X onClick={() => toggle()}>X</X>
				</div>
					{!spinner ? (
						<div className="modal-body">
							{
								comicListResult.length > 0 ? (
									comicListResult.map((comic, i) => {								
										return (
											<CardModal key={i} name={heroName} comic={comic} />
										)
									})
								) : (
									<div>
										<NameStar>{heroName}</NameStar>
										<NameStar>don't have any comics</NameStar>
										<img src={hydra} alt="No found" width={120} />
									</div>
								)								
							}
						</div>
					) : (
						<div style={{ display: 'flex', justifyContent: 'center' }}>
							<div className="sp sp-3balls"></div>
						</div>						
					)}
				</div>
			</div>
		);
	}
}

Modal.propTypes = {
	modal: PropTypes.bool.isRequired,
	heroName: PropTypes.string.isRequired,
	comicList: PropTypes.array.isRequired,    
    toggle: PropTypes.func.isRequired,
};

export default Modal