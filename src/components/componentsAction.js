export const getData = async (comicList) => {    	
    const arrayData = await Promise.all(comicList.map(async (file) => {
        return await fetch(file)			
    }));

    const arrayDataJson = await Promise.all(arrayData.map(async (file) => {
        return await file.json()			
    }));

    const comicListResult = arrayDataJson.map(element => element.data.results[0])
    comicListResult.sort((a, b) => {			
        return (b.dates[0].date - a.dates[0].date)
    })
    return comicListResult
}