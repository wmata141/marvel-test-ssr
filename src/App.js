import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import HomePage from './modules/home/homePage';
import DetailPage from './modules/detail/detailPage';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" search='?character=hero' component={HomePage} />
        <Route path="/detailComic/:heroId" component={DetailPage} />        
        <Redirect from="*" to="/" />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
